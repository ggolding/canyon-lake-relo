<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16348386-17']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<div id="container">
<div id="body">
  <div id="top-contact-bg">
    <div id="top-contact-cnt">
      <h2>800.510.2212</h2>&nbsp;&nbsp;&nbsp;<span>Please Call for an Appointment</span>
      <div id="social-media">
        <img src="<?=$a;?>images/twitter.png" alt="Canyon Lake Relo Twitter" />
        <img src="<?=$a;?>images/facebook.png" alt="Canyon Lake Relo Facebook" />
      </div>
    </div>
  </div>
  <div id="top-area">
    <a href="<?=$home?>"><img class="no-red-a" src="<?=$a?>images/Prudential-Logo.png" alt="Canyon Lake Relo" /></a>
  </div><!-- END #top-area -->
  
  <?php include ($a . 'includes/4.navbar.php'); ?>