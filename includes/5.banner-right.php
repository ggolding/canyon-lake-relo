<div id="banner-right">
  <h4 class="oswald">FIND YOUR DREAM HOME</h4>
  <div class="mb20"></div>
  <form>
  
    <label>Price </label>
    
    <div class="inline first">
      <input type="text" placeholder="From" />
    </div>
    <div class="inline last">
      <input type="text" placeholder="To" />
    </div>
    
    <label>Location</label>
    <input type="text" placeholder="City or Zip" />
    
    
    
    <div class="inline first">
      <label>Min Bathrooms</label>
      <input type="text" placeholder="ex: 2.5" />
    </div>
    <div class="inline last">
      <label>Min Bedrooms</label>
      <input type="text" placeholder="ex: 4" />
    </div>
    
    <br />
    <br />
    
    <input type="submit" class="button medium full-width" />
  
  </form>
</div>
</div><!-- end #banner-cnt -->