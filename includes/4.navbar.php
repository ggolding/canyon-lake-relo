<?php
$active[$current] = "active";
$active[$subCurrent] = "active";
?>

<div id="navbar">
  <ul class="navbar">
    <a href="<?=$home;?>"><li class="navhome <?=$active[1]?>">Home</li></a>
    <a href="<?=$waterskiing;?>"><li class="navski <?=$active[2]?>">Water Skiing</li></a>
    <a href="<?=$golf;?>"><li class="navgolf <?=$active[3]?>">Golf</li></a>
    <a href="<?=$beachesparks;?>"><li class="navpark <?=$active[4]?>">Beaches/Parks</li></a>
    <a href="<?=$lodge;?>"><li class="navlodge <?=$active[5]?>">Lodge</li></a>
    <a href="<?=$clubs;?>"><li class="navclub <?=$active[6]?>">Clubs</li></a>
    <a href="<?=$listings;?>"><li class="navlist <?=$active[7]?>">Listings</li></a>
    <a href="<?=$contact;?>"><li class="navcont <?=$active[8]?>">Contact</li></a>
  </ul>
</div>
