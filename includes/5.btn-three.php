<div class="row">
  <a href="#">
    <span class="btn-three radius8 blue-bg mr11">
      <img class="right" src="<?=$a;?>images/plus-button.png" />
      <div class="clear"></div>
      <h2 class="white fifty">Staging</h2>
      <p class="semibold white twelve">Home staging is a way of showing a house in the best possible light to prospective buyers in order to sell it faster and for a better price.</p>
    </span>
  </a>
  <a href="<?=$redesign;?>">
    <span class="btn-three radius8 green-bg mr11">
      <img class="right" src="<?=$a;?>images/plus-button.png" />
      <div class="clear"></div>
      <h2 class="white fifty">Redesign</h2>
      <p class="semibold white twelve">ReDesign your current living space using what you already own.</p><br />
  </span>
  </a>
  <a href="#">
    <span class="btn-three radius8 red-bg">
      <img class="right" src="<?=$a;?>images/plus-button.png" />
      <div class="clear"></div>
      <h2 class="white fifty">Services</h2>
      <p class="semibold white twelve">Staging for selling. Home Staging Do-It-Yourself Plan. Vacant Home Staging. Redesign your home. Color Consultations.</p>
    </span>
  </a>
</div>

<hr />