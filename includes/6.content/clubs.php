<div class="row">
  <div class="twelve columns">
    <h1>Clubs</h1>
  </div>
</div>

<div class="row">
  <div class="eight columns">
    
    <h3>Art Association</h3>
    <p>The Canyon Lake Art Association was formed with the primary purpose of promoting understanding and appreciation of the visual arts in the community, encouraging advance study in the field of art and providing exhibits and programs for local artists.</p>
    
    <h3>Amateur Radio Service</h3>
    <p>Members of the Canyon Lake Amateur Radio Service (CLARS) are licensed amateur radio operators (Hams) and others who have joined together to provide communication in an emergency.</p>
    
    <h3>Bosom Buddies</h3>
    <p>Dedicated to educating women about breast cancer, members of Bosom Buddies meet with newly diagnosed women to share hopes, fears and concerns.</p>
    
    <h3>Bassmasters</h3>
    <p>The Canyon Lake Bassmasters are affiliated with the Bass Anglers Sportsmen's Society (BASS) and all members are required to be members of BASS as well as residents of Canyon Lake.</p>
    
    <h3>Bible Fellowship</h3>
    <p>The Canyon Lake Bible Fellowship is a non-denominational group whose mission statement is "Knowing God and making Him known" through the study of scripture and compassionate outreach to the community.</p>
    
    <h3>Bridge Groups</h3>
    <p>A Mixed Couples Duplicate Bridge group meets at Blue Bird Hall at 6 p.m. on the first, third and fifth Tuesdays of the month.</p>
    
    <h3>Car Club</h3>
    <p>Canyon Lakers who enjoy classic and custom automobiles meet the first Thursday of each month at 5:30 p.m. at the Lodge to discuss upcoming events, listen to guest speakers and share automotive experiences.</p>
    
    <h3>Chair Volleyball</h3>
    <p>The Canyon Lake Chair Volleyball Club for Seniors promotes fitness, fun, friendship and the spirit of competition through the organized playing of Chair Volleyball.</p>
    
    <h3>Campground Club</h3>
    <p>The objectives of the club are to enhance the aesthetics of Happy Camp using fundraisers and even a work group and to be a family club where people of all ages can enjoy the outdoors.</p>
    
    <h3>Choraleers</h3>
    <p>The Canyon Lake Choraleers is a vocal group of men and women who enjoy choral singing and can carry a tune independently.</p>
    
    <h3>CLAMS</h3>
    <p>The Canyon Lake Association of Men (CLAMS) was organized in March of 1973 to promote fellowship among the male populace of Canyon Lake.</p>
    
    <h3>CLAWS</h3>
    <p>CLAWS invites all women in the community to join them in playing Bunco on the second Wednesday of each month in the Magnolia Room.</p>
    
    <h3>Co-ed Softball</h3>
    <p>The Co-ed Softball Club has 10 or 12 teams and is open winter and spring seasons to men and women of at least 21 years of age who want to play and have fun.</p>
    
    <h3>Community Theatre</h3>
    <p>The purpose of this non-profit organization is to provide community residents with access to the performing arts by providing a platform and showcase for local talent.</p>
    
    <h3>Cotillion</h3>
    <p>Continuing the tradition begun in 1989 by the late Margene Ziff, director Ed Strong conducts meetings of the Cotillion on a monthly basis, offering lessons in etiquette and ballroom,waltz, fox trot, cha-cha and swing dancing for boys and girls in 4th through 8th grades.</p>
    
    
    <h3>Cub Scouts of America</h3>
    <p>Together they have fun with a purpose - along with the activities the aim of Cub Scouting is to help boys grow into good citizens who are strong in character and physically fit.</p>
    
    <h3>Dancercize</h3>
    <p>Dancercize, a low-impact exercise class, meets at the Senior Center on Thursdays from 9:30 to 11 a.m. and involves easy stretching exercises starting with upper body, then floor exercises before a half hour of line dancing.</p>
    
    <h3>Equestrian Club</h3>
    <p>The Equestrian Club was formed in 1970 to provide amenities for local equestrian enthusiasts. Horse shows, potlucks, and trail rides are just a few of the many activities the club sponsors each year.</p>
    
    <h3>Friends of the Library</h3>
    <p>Friends of the Library provide organized support to the library. Income for Friends is derived from contributing members, donations from other clubs, fundraisers and an ongoing book sale.</p>
    
    <h3>Fine Arts Guild</h3>
    <p>The purpose of the Fine Arts Guild is to bring quality entertainment to Canyon Lake by presenting several concerts throughout the year. The very first concert took place in January of 2001.</p>
    
    <h3>Fitness and Triathlon Club</h3>
    <p>The club's vision is to promote a healthy lifestyle through swimming, cycling, running and other fitness-related activities. Canyon Lake is the perfect venue for all of these.</p>
    
    <h3>Garden Club</h3>
    <p>The Canyon Lake Garden Club is an informal group comprised of anyone who would like to get together with other gardeners in the community to discuss the trials and tribulations--as well as the joys--of gardening. </p>
    
    <h3>Girl Scouts of the United States</h3>
    <p>The Girl Scout organization prides itself on building courage, confidence and character and in today's girls and tomorrow's women. Activities include troop meetings, individual opportunities, local events, field trips, community service, outdoor experiences and special awards and earned recognition.</p>
    
    <h3>Men's Golf Club</h3>
    <p>The Men's Golf Club is affiliated with the Southern California Golf Association, which computes all official handicaps, organizes inter- team play and mails its publication, Fore Magazine, to club members. </p>
    
    <h3>Women's Golf Club</h3>
    <p>The Women's Golf Club was established in 1971 and is a member of the Women's Southern California Golf Association, which provides all official handicapping services, organizes team play competitions and allows members to participate in various golf tournaments throughout Southern California. </p>
    
    <h3>Family Golf Club</h3>
    <p>The goals of the Canyon Lake Family Golf Club are to promote fun, fair and competitive golf for men, women and juniors, along with supporting the community of Canyon Lake.</p>
    
    <h3>9ers Golf Club</h3>
    <p>The Canyon Lake Ladies Niners Golf Club is open to all women who want to play nine holes of golf.</p>
    
    <h3>Hand and Foot Canasta</h3>
    <p>A group of Canyon Lake women who enjoy playing Hand and Foot Canasta meets each Monday at the Lodge.</p>
    
    <h3>Home Owners Club</h3>
    <p>Organized in March 1972, this club was formed to promote and protect the best interests of Canyon Lake homeowners and property owners by encouraging actions which would make the community a desirable place in which to live and own property.</p>
    
    <h3>Jr. Women's Club</h3>
    <p>The Junior Women's Club is open to women with an interest in serving Canyon Lake. The goal is to "Build a Better Community" with emphasis on youth.</p>
    
    <h3>Hydrofoil Club</h3>
    <p>Canyon Lakers enjoy watching demonstrations by the Hydrofoil Club during Taco Tuesdays and appreciate the group's other events throughout the year.</p>
    
    <h3>Kids Club</h3>
    <p>The Kids Club is a program in which young children and their parents or grandparents can interact in a safe, structured and caring environment and enjoy a morning of crafts, music, sports, exercise, games and a snack for just $2 per session.</p>
    
    <h3>Lighthouse Players</h3>
    <p>The Lighthouse Players, an ensemble of local performers, are dedicated to serving as a "beacon" for benevolent causes and fundraising events. </p>
    
    <h3>Lions Club</h3>
    <p>The Canyon Lake Lions Club is one of more than 40,000 Lions Clubs with 1,600,000 members in 200 countries.</p>
    
    <h3>Lioness Club</h3>
    <p>The Canyon Lake Lioness Club was sponsored by the Canyon Lake Lions in 1979 and is part of the world's largest service organizations, the Lions Club International Foundation.</p>
    
    <h3>Little League</h3>
    <p>Boys and girls ages 5 through 16 are invited to join the Canyon Lake Little League. The League's objective is to instill in the children of the community the ideals of good sportsmanship, honesty, loyalty, courage and respect for authority, so that they can grow to be decent, healthy and trustworthy citizens.</p>
    
    <h3>Men's Softball Association</h3>
    <p>Canyon Lake men 18 and over who enjoy playing ball are invited to join the Men's Softball Association. The league consists of eight teams that play on Wednesday nights during spring and fall seasons.</p>
    
    <h3>Mommy and Me</h3>
    <p>Two sessions of Mommy and Me meet during the year for fun-filled times. Fall and winter classes begin in September and winter and spring classes begin in January.</p>
    
    <h3>Pinochle Club</h3>
    <p>It was at a fireside gathering on a Roadrunner outing more than 20 years ago that the idea of a Pinochle Club was born.</p>
    
    <h3>Motorcycle Club</h3>
    <p>The Canyon Lake Motorcycle Club is a community-wide organization designed to promote safe and respectful motorcycle riding for all residents who own street-legal motorcycles of all makes and models.</p>
    
    <h3>Panhellenic Alumnae</h3>
    <p>Canyon Lake Panhellenic Alumnae was formed in1989 to unite the Alumnae of the 26 national sororities affiliated with the National Panhellenic Conference and encourages high school women graduates who are planning on attending a four year University to seek sorority membership as part of their college life.</p>
    
    <h3>Republican Women Federated</h3>
    <p>CLRWF was formed in October 2009 by a small group of women who were not happy with the state of the country. They formed a club and became a sanctioned by the POA and chartered by the National Federation at the same time.</p>
    
    <h3>Roadrunners</h3>
    <p>The group plans a trip of three to five days each month, except in the summer, and the trips are always on week days.</p>
    
    <h3>Street Carts of Canyon Lake</h3>
    <p>Street Carts of Canyon Lake is a non-profit organization founded to raise money for local families in need through golf cart rallies, drive-in movie nights, poker runs, toy and food drives and participation in community events such as Fiesta Day and the Parade of Lights.</p>
    
    <h3>Ski Club</h3>
    <p>From Barefoot Ski Tournaments to Breakfast with Santa, the Canyon Lake Ski Club is active all year long.</p>
    
    <h3>Tennis Club</h3>
    <p>Organized in 1974, the purpose of the Tennis Club is to promote tennis, to meet other tennis players and to improve tennis skills.</p>
    
    <h3>Travel Club</h3>
    <p>The Canyon Lake Travel Club originated in March 1978. From its inception, its purpose has been to plan, promote and conduct tours for its members for their recreation and enjoyment as a group. </p>
    
    <h3>Torah Club</h3>
    <p>This club promotes the principles of Torah in a non-denominational way and applies the principles of Torah to performing good deeds and charitable acts for the community at large.</p>
    
    <h3>Tuesday Work Group</h3>
    <p>The Tuesday Work Group consists of more than 60 volunteers dedicated to improving the Canyon Lake Golf Course.</p>
    
    <h3>Twirlers Square Dance Club</h3>
    <p>Modern square dancing is a mental and physical challenge set to both oldies and current music. Walking in rhythm to the music keeps dancers physically fit, and reacting quickly to the caller keep dancers mentally on their toes.</p>
    
    <h3>Wakeboard Club</h3>
    <p>The Canyon Lake Wakeboard Club is Southern California's premier wakeboard connection, founded in 1998 by a group of wakeboard enthusiasts committed to promoting the sport of wakeboarding.</p>
    
    <h3>Woman's Club</h3>
    <p>The Canyon Lake Woman's Club is a non-profit, non-partisan philanthropic service organization open to all women.</p>
    
    
    <h3>Women's Connection</h3>
    <p>The Canyon Lake Women's Connection is a local chapter of Stonecroft Ministries, Christian Women's Club, an international non-profit, non-denominational religious organization.</p>
    
    <h3>Yacht Club</h3>
    <p>The Canyon Lake Yacht Club was formed in the summer of 1990 for the benefit of all property owners who use the lake.</p>
  </div>
  
  <div class="four columns panel green">
    <h3 class="white oswald">Contact Us</h3>
    <h4 class="white">800.510.2212</h4>
  </div>
</div>