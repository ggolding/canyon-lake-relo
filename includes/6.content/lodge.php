<div class="row">
  <div class="twelve columns">
    <h1>Canyon Lake Lodge</h1>
  </div>
</div>

<div class="row">
  <div class="eight columns">
    <img class="border mb20" src="<?=$a;?>images/canyon-lake-lodge.jpg" />
    <p>Recently rebuilt, the lodge features a restaurant, meeting rooms, a lounge, and a multi-purpose room. Sitting around the large L-shaped swimming pool you feel like you are at a resort. Surrounded by palm trees, you will enjoy the panoramic view of the lake and homes. The pool is heated and features a unique, solar-assisted system. You can lounge by the pool under one of the many shade structures or you might enjoy a water aerobics class. There are 3 lap swimming lanes and a separate toddler pool. Showers and changing facilities, along with restroom facilities, are available. From the lodge, it's an easy walk to sunset beach.</p>
  </div>
  <div class="four columns panel green">
    <h3 class="white oswald">Contact Us</h3>
    <h4 class="white">800.510.2212</h4>
  </div>
</div>