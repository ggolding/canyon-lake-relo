<div class="row">
  <div class="twelve columns">
    <h1>Beaches and Parks</h1>
  </div>
</div>

<div class="row">
  <div class="twelve columns">
    <img class="border mb15" src="<?=$a;?>images/beaches-parks/happy-camp.jpg" />
    <h2>Happy Camp</h2>
    <p>Happy Camp includes 7.5 acres with 90 camp sites. On 37 of the sites you will find complete hook-ups for water, sewer, electric, and television. The remaining 53 sites are equipped with electric and water. You will also enjoy having showers, restrooms, washers, dryers, and an ice machine. There is a dump site for all campers. A full time manager lives on the premises. The 5 docks can accommodate up to 45 boats at any given time. The swimming beach is 350 feet long. The campground is a great place for fishing, playing horseshoes, volleyball, and windsurfing in the protected, 5 miles an hour zone. Water skiing is only 200 feet away. This works great for family and friends who want to stay in their own motor home. The gas docks are also at Happy Camp to serve the approximately 2,000 registered boat owners of Canyon Lake.</p>
  </div>
</div>

<div class="row">
  <div class="eight columns">
    
    <div class="row">
      <div class="twelve columns">
        <h3>North Ski Area</h3>
        <img class="border mb10" src="<?=$a;?>images/beaches-parks/north-ski-area.jpg" />
        <p>Separated from the main lake by a causeway is the North Ski Area. Here you will find a spectator platform, a waiting dock, a restroom, and a starting dock. This area is set up for the tournament skier and slalom, trick, and barefoot competition. There are separate parking and launching facilities. Behind the competition area there is a another small area for recreational skiing and wakeboarding.</p>
      </div>
    </div>
    
    <div class="row">
      <div class="twelve columns">
        <h3>Roadrunner Park</h3>
        <p>Roadrunner Park was established by the Canyon Lake RV group. It is 300 feet long and 150 feet wide. It has become a popular destination for boaters with its dock, volleyball court, horseshoe pits, concrete, covered picnic area, and barbecue grills. Electricity is available.</p>
      </div>
    </div>
    
    
    
  </div>
  <div class="four columns">
  
    <div class="row">
      <div class="twelve columns">
        <h3>Sunset Beach</h3>
        <p>Sunset Beach is the largest and most popular swimming and sunning beach park. Located in front of the lodge, it has 550 feet of water frontage, a playground and a protected swimming area. It is an easy walk to the pool and snack bar.</p>
      </div>
    </div>
        
    <div class="row">
      <div class="twelve columns">
        <h3>Harrelson Memorial Park</h3>
        <p>One of the most enchanting and beautiful parks in Canyon Lake. The park boasts wooden steps that take you to the water's edge, a dock, and picnic facilities with seating and tables. A towering flag pole topped by a golden eagle is visible above the trees. This park is dedicated to the memory of Troy Lee Harrelson, a young skiing accident victim.</p>
      </div>
    </div>   
    
    <div class="row">
      <div class="twelve columns">
        <h3>Moonstone Beach</h3>
        <p>Moonstone Beach is a small day use park. The park offers swings, portable restrooms, three picnic tables, a wooden ramada, two metal beach umbrellas, and a volleyball court. There are many trees and a swimming cove out of the boat traffic.</p>
      </div>
    </div> 
    
  </div>
</div>