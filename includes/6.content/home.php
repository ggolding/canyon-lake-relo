<div class="row">
  <div class="btn-three">
    <img src="images/home-small-2.jpg" />
    <h4>Buy a Home</h4>
    <p>Text about Buy a Home. Text about Buy a Home. Text about Buy a Home. Text about Buy a Home. Text about Buy a Home. </p>
    <a href="#" class="button">Read More...</a>
  </div>
  <div class="btn-three mid">
    <img src="images/home-small-3.jpg" />
    <h4>Sell a Home</h4>
    <p>Text about Sell a Home. Text about Sell a Home. Text about Sell a Home. Text about Sell a Home. </p>
    <a href="#" class="button">Read More...</a>
  </div>
  <div class="btn-three">
    <img src="images/home-small-1.jpg" />
    <h4>Why Canyon Lake?</h4>
    <p>Text about Why Canyon Lake. Text about Why Canyon Lake. Text about Why Canyon Lake. </p>
    <a href="#" class="button">Read More...</a>
  </div>
</div>

<br />
<div class="clear25;">&nbsp;</div>
<br />

<div class="row">
  <div class="eight columns">
    <h3>Current Listings</h3>
    <div class="row listing">
      <div class="four columns"><img src="images/listings/22600-Calcutta.jpg" /></div>
      <div class="eight columns">
        <span class="listing-address">22600 Calcutta Drive, Canyon Lake</span>
        <br />
        <p>Text about the current listing. Text about the current listing. Text about the current listing. Text about the current listing. Text about the current listing.</p>
      </div>
    </div>
    <div class="row listing">
      <div class="four columns"><img src="images/listings/30430-Point-Marina.jpg" /></div>
      <div class="eight columns">
        <span class="listing-address">30430 Point Marina Drive, Canyon Lake</span>
        <br />
        <p>Text about the current listing. Text about the current listing. Text about the current listing. Text about the current listing. Text about the current listing.</p>
      </div>
    </div>
    <div class="row listing">
      <div class="four columns"><img src="images/listings/29883-Ketch.jpg" /></div>
      <div class="eight columns">
        <span class="listing-address">29883 Ketch Drive, Canyon Lake</span>
        <br />
        <p>Text about the current listing. Text about the current listing. Text about the current listing. Text about the current listing. Text about the current listing.</p>
      </div>
    </div>
  </div>
  <div class="four columns">
    <h3>About Us</h3>
    <div class="row">
      <div class="five columns"><img src="images/janet-pigeon.jpg" /></div>
      <div class="seven columns"><h4>Janet Pigeon</h4></div>
    </div>
    <div class="row">
      <div class="twelve columns">
        <p>Text about Janet Pigeon. Text about Janet Pigeon. Text about Janet Pigeon. Text about Janet Pigeon. Text about Janet Pigeon. Text about Janet Pigeon. Text about Janet Pigeon. </p>
      </div>
    </div>
    <div class="row">
      <div class="five columns"><img src="images/charlene-rideout.jpg" /></div>
      <div class="seven columns"><h4>Charlene Rideout</h4></div>
    </div>
    <div class="row">
      <div class="twelve columns">
        <p>Text about Charlene Rideout. Text about Charlene Rideout. Text about Charlene Rideout. Text about Charlene Rideout. Text about Charlene Rideout. Text about Charlene Rideout. </p>
      </div>
    </div>
  </div>
</div>