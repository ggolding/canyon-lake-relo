  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />
  
  <!-- CSS Files -->
  <link rel="stylesheet" href="<?=$a?>stylesheets/foundation.min.css">
  <link rel="stylesheet" href="<?=$a?>stylesheets/app.css">
  <link rel="stylesheet" href="<?=$a?>stylesheets/styles.css">
  <link rel="stylesheet" href="<?=$a?>stylesheets/fc-webicons.css">

  <script src="<?=$a?>javascripts/modernizr.foundation.js"></script>

  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    <script src="<?=$a?>javascripts/html5.js"></script>
  <![endif]-->

  <link rel="icon" href="<?=$a?>favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="<?=$a?>favicon.ico" type="image/x-icon" />