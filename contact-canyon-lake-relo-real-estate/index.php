<?php 
	$a = '../';
	$current = 8;
	$thisPageContent = 'contact.php';

	  include ($a . 'includes/0.seo.php');
	
	$SEOTitle = $Thome;
	$SEOKeywords = $Khome;
	$SEODescription = $Dhome;
	
	  include ($a . 'includes/0.sitelinks.php');
	  include ($a . 'includes/1.doctype.php');
?>
<head>
<?php include ($a . 'includes/2.head.php'); ?>

  <title><?=$SEOTitle?></title>
		
  <meta name="keywords" content="<?=$SEOKeywords?>">	
  <meta name="description" content="<?=$SEODescription?>">
  
</head>

<body>

<?php 
  include ($a . 'includes/3.top.php'); 
  
  ## '4.navbar.php' is included in '3.top.php'

  include ($a . 'includes/5.banner/' . $thisPageContent );
  include ($a . 'includes/5.banner-right.php');
  include ($a . 'includes/6.content/' . $thisPageContent );
?>

<?php
  include ($a . 'includes/7.footer.php');
  include ($a . 'includes/8.bottom.php'); 
?>

</body>
</html>