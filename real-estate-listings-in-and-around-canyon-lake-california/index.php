<?php 
	$a = '../';
	$current = 7;
	$thisPageContent = 'listings.php';

	  include ($a . 'includes/0.seo.php');
	
	$SEOTitle = $Thome;
	$SEOKeywords = $Khome;
	$SEODescription = $Dhome;
	
	  include ($a . 'includes/0.sitelinks.php');
	  include ($a . 'includes/1.doctype.php');
?>
<head>
<?php include ($a . 'includes/2.head.php'); ?>

  <title><?=$SEOTitle?></title>
		
  <meta name="keywords" content="<?=$SEOKeywords?>">	
  <meta name="description" content="<?=$SEODescription?>">
</head>

<body>

<?php 
  include ($a . 'includes/3.top.php'); 
  include ($a . 'includes/6.content/' . $thisPageContent );
?>

<?php
  include ($a . 'includes/7.footer.php');
  include ($a . 'includes/8.bottom.php'); 
?>

</body>
</html>